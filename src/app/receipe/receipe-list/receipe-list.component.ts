import { Component, OnInit } from '@angular/core';

import { Receipe } from './receipe.model';


@Component({
  selector: 'app-receipe-list',
  templateUrl: './receipe-list.component.html',
  styleUrls: ['./receipe-list.component.css']
})
export class ReceipeListComponent implements OnInit {


   receipes : Receipe[] = [
   new Receipe('chicken', 'this is starter dish', 'https://cdn.pixabay.com/photo/2016/12/26/17/28/spaghetti-1932466__340.jpg'),
   new Receipe('chicken1', 'this is starter dish', 'https://cdn.pixabay.com/photo/2016/12/26/17/28/spaghetti-1932466__340.jpg'),

   ];

  constructor() { }

  ngOnInit(): void {
  }

}
