import { Component, OnInit } from '@angular/core';
import { Ingredients } from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  ingredients : Ingredients[] = [
  new Ingredients('Tomoto', 2),
  new Ingredients('Onioin', 2),
  new Ingredients('Carrot', 2),
  new Ingredients('Beans', 2),
  new Ingredients('Beans', 2)
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
